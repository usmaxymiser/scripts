module.exports = function (grunt) {
    // Project configuration.
    require('load-grunt-tasks')(grunt);
    var scriptList = [
        'snippets/core.js',
        'snippets/snippet_style.js',
        'snippets/deferred.js',
        'snippets/EventManager.js',
        'snippets/printFix.js',
        'snippets/wait-for.js',
        'snippets/request.js',
        'snippets/hooks.js',
        'snippets/Campaign.js'
    ];
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        version: {
            project: {
                src: ['package.json']
            }
        },
        concat: {
            options: {
                separator: '',
                process: function (src, filepath) {
                    var _vers = /@version (\d+(\.\d)*)/i;
                    var _nam = /@name ([a-zA-Z]*)/i;
                    var _version = src.match(_vers);
                    var __names = src.match(_nam);
                    var version = _version ? _version[0] : 'not defined'
                    return ' \n/* !! ' + __names[0] + ' ' + version + ' */' + src;
                    //return '\n' + src
                }
            },
            dist: {
                src: scriptList,
                dest: 'snippets/snippets_temp.js'
            }
        },
        'string-replace': {
            dist: {
                files: {
                    'snippets/snippets_temp.js': 'snippets/snippets_temp.js'
                },
                options: {
                    replacements: [{
                        pattern: /(;\s+[(]function)|([;][(]function)/g,
                        replacement: "(function"
                    }]
                }
            }
        },
        uglify: {
            options: {
                compress: {
                    drop_console: true
                },
                preserveComments: function (testing, test2) {
                    return (/!!/).test(test2.value)

                },
                banner: '/*! <%= grunt.file.readJSON("package.json").makeVersion %> - <%= grunt.template.today("yyyy-mm-dd h:MM:ss") %> */\n'
            },
            source: {
                files: {
                    'snippets_min/snippets_min.js': [
                        'snippets/snippets_temp.js'
                    ]
                }
            }
        },
        //gitpull: {
        //    your_target: {
        //        options: {
        //            remote:'git@bitbucket.org:snippets/usmaxymiser/nMrg/snippet_style.git',
        //            cwd: "js/snippet_style"
        //        }
        //
        //    }
        //
        //    //
        //},
        make:{
            default:{
                option:{
                    type:false
                }
            },
            minor:{
                option:{
                    type:':minor'
                }
            },
            major:{
                option:{
                    type:':major'
                }
            }
        }

    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.registerTask('updateVersion', function(type) {
        var projectFile = "package.json";
        if (!grunt.file.exists(projectFile)) {
            grunt.log.error("file " + projectFile + " not found");
            return true;//return false to abort the execution
        }
        var project = grunt.file.readJSON(projectFile);//get file as json object
        var _newVersion = project.makeVersion.split('.');
        if (type){
            if (type=='default'){
                _newVersion[2] = parseFloat(_newVersion[2])+1;
            }
            else if (type=='minor'){
                _newVersion[1] = parseFloat(_newVersion[1])+1;
                _newVersion[2]=0
            }else if(type=='major'){
                _newVersion[0] =  parseFloat(_newVersion[0])+1;
                _newVersion[1]=0;
                _newVersion[2]=0;
            }
        }else{
            _newVersion[2] = parseFloat(_newVersion[2])+1;
        }
          project['makeVersion']= _newVersion.join('.').toString()

        grunt.file.write(projectFile, JSON.stringify(project, null, 2));//serialize it back to file

    });
    grunt.registerTask('replaceRegNew', function() {
        var projectFile = "snippets_min/snippets_min.js";
        if (!grunt.file.exists(projectFile)) {
            grunt.log.error("file " + projectFile + " not found");
            return true;//return false to abort the execution
        }
        var project = grunt.file.read(projectFile);//get file as json object

        var newProj = project.replace(/\/\* !!/g, '\n/* !!');
        grunt.file.write(projectFile, newProj);//serialize it back to file

    });
    grunt.registerTask('make', function(type){
            var _type='';
            if (type){
                _type=':'+type
            }
            grunt.task.run(['updateVersion'+_type,'concat', 'string-replace', 'uglify','replaceRegNew']);
        }
    );


};