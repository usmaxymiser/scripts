/**
 * Created by ocherkashenko on 07.07.2015.
 * @name library
 * @version 0.0.2
 */
(function () {
    if (typeof mmcore.snippets == 'undefined') {
        mmcore.snippets = {};
    }
    var namespace = mmcore.snippets;
    var class2type = {};

    namespace.extend = function () {
        var options, name, target = arguments[0] || {},
            i = 1,
            length = arguments.length;
        // Extend jQuery itself if only one argument is passed
        if (i === length) {
            target = this;
            i--;
        }

        for (; i < length; i++) {
            if ((options = arguments[i]) != null) {
                for (name in options) {
                    target[name] = options[name];
                }
            }
        }
        // Return the modified object
        return target;
    };
    namespace.extend({
        /**
         * @param obj
         * @param callback
         * @returns {*}
         */
        each: function (obj, callback) {
            var i = 0,
                length = obj.length,
                isArray = isArraylike(obj);

            if (isArray) {
                for (; i < length; i++) {
                    if (callback.call(obj[i], i, obj[i]) === false) {
                        break;
                    }
                }
            } else {
                for (i in obj) {
                    if (callback.call(obj[i], i, obj[i]) === false) {
                        break;
                    }
                }
            }

            return obj;
        },
        /**
         * @param {*} itemToCheck
         * @param {Array} arr
         * @returns {boolean}
         */
        inArray: function (elem, list) {
            var i = 0,
                len = list.length;
            for ( ; i < len; i++ ) {
                if ( list[i] === elem ) {
                    return i;
                }
            }
            return -1;
        },
        /**
         * @param elems
         * @param callback
         * @param invert
         * @returns {Array}
         */
        grep: function (elems, callback, invert) {
            var callbackInverse,
                matches = [],
                i = 0,
                length = elems.length,
                callbackExpect = !invert;

            // Go through the array, only saving the items
            // that pass the validator function
            for (; i < length; i++) {
                callbackInverse = !callback(elems[i], i);
                if (callbackInverse !== callbackExpect) {
                    matches.push(elems[i]);
                }
            }
            return matches;
        },
        /**
         * @param obj {Object}
         * @returns string
         */
        type: function (obj) {
            return obj == null ?
                String(obj) :
            class2type[Object.prototype.toString.call(obj)] || 'object';
        }
    });

    // Populate the class2type map
    namespace.each('Boolean Number String Function Array Date RegExp Object Error'.split(' '),
        function (i, name) {
            class2type['[object ' + name + ']'] = name.toLowerCase();
        });

    function isArraylike(obj) {

        // Support: iOS 8.2 (not reproducible in simulator)
        // `in` check used to prevent JIT error (gh-2145)
        // hasOwn isn't used here due to false negatives
        // regarding Nodelist length in IE
        var length = 'length' in obj && obj.length,
            type = namespace.type(obj);

        if (type === 'function' || obj != null && obj === obj.window) {
            return false;
        }

        return type === 'array' || length === 0 ||
            typeof length === 'number' && length > 0 && ( length - 1 ) in obj;
    }
}());