/**
 *
 * @name Style
 * @version 0.1.8
 *
 */
;
(function (namespace) {
	namespace.Style = function (css) {
		var style;
		var uniqueId = function (length) {
			var id = '';
			do {
				id += Math.random().toString(36).substr(2);
			} while (id.length < length);
			return id.substr(-length);
		};

		var init = function () {
			style = document.createElement("style");
			style.type = "text/css";
			style.media = "screen";
			style.className = 'mm_' + uniqueId(15);
			document.getElementsByTagName('head')[0].appendChild(style);
			attach(css);
		};

		var getStyle = function (style) {
			if (style && !style.parentNode) {
				return document.querySelector('.' + style.className);
			}
			return style;

		};
		var attach = function (css) {
			style = getStyle(style);
			if (style && css) {
				if (style.styleSheet) {
					style.styleSheet.cssText += css;
				} else {
					style.innerHTML += css;
				}
			}
		};

		var detach = function () {
			style = getStyle(style);
			if (style) {
				style.parentNode.removeChild(style);
				style = null;
			}
		};

		init();

		return {
			attach: attach,
			detach: detach
		}
	}
}(mmcore.snippets));