/**
 * @name Request
 * @version 0.0.2
 *
 */
;
(function (namespace, Deferred) {
	var ACTION_TRACKING_PAGE_ID = 'mmevents'; //Default page
	var REQUEST_TIMEOUT_EVENTS = 2E3;  // In milliseconds.
	var REQUEST_TIMEOUT_PAGES = 1E4;  // In milliseconds.

	namespace.request = function (pageId, isSynchronous, timeout) {
		var request = Deferred();

		pageId = pageId || ACTION_TRACKING_PAGE_ID;
		timeout = timeout || (pageId == ACTION_TRACKING_PAGE_ID ? REQUEST_TIMEOUT_EVENTS : REQUEST_TIMEOUT_PAGES);

		namespace.SetPageID(pageId);
		namespace._async = !isSynchronous;
		namespace.CGRequest(request.resolve);
		setTimeout(function () {
			request.reject('timeout error');
		}, timeout);
		return request.promise()
	};
}(mmcore, mmcore.Deferred));