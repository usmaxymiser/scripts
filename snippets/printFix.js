/**
 * @name printFix
 * @version 0.0.2
 *
 */
;
(function () {
    function bind(el, func) {
        if (el.addEventListener) {
            el.addEventListener('beforeprint', func, false);
        } else if (el.attachEvent) {
            el.attachEvent('onbeforeprint', func);
        }
    }

    bind(window, function () {
        var nodes = document.querySelectorAll('script[id^="' + mmcore.cprefix + '"][src]'),
            i = nodes.length;
        while (i--) {
            nodes[i].removeAttribute('src');
        }
    });
})();