/**
 * Created by ocherkashenko on 17.08.2015.
 */
mmcore.snippets.Storage = function (nameStorage) {
	var get = function () {
		var cookie = mmcore.GetCookie(nameStorage, 1) || 'null';
		var out;
		try {
			out = JSON.parse(cookie);
		} catch (err) {
			mmcore.EH(new Error('Storage get: ' + err));
		}


	};
	var set = function (data) {
		try {
			mmcore.SetCookie(nameStorage, JSON.stringify(data), 365, 1);
		} catch (err) {
			mmcore.EH(new Error('Storage set: ' + err));
		}
	};
	var addItem = function (name, val) {
		var data = this.get();
		data[name] = val;
		this.set(data);
	};
	var getItem = function (name, val) {
		var data = this.get();
		return data[name]
	};
	return {
		get: get,
		set: set,
		addItem: addItem,
		getItem: getItem
	}
};
//hook
mmcore._Clear = (function (fn) {
	return function () {
		var pc = mmcore._vars.uat;
		var out = fn.apply(this, arguments);
		mmcore._vars.uat = pc;
		return out;
	};
}(mmcore._Clear));
(function () {
		var CRITERIA = 'mm_criteria',
			storage = mmcore.snippets.Storage(CRITERIA);
		mmcore.persCriteria = {};
		mmcore.listDefPC = [];
		mmcore.SetPersCriterion = (function (fn) {
			return function (name, val) {
				storage.add(name, val);
				mmcore.persCriteria[name] = val;
				return fn.apply(this, arguments);
			};
		}(mmcore.SetPersCriterion));

		mmcore.createPC = function (name, defaultValue) {
			var val = storage.getItem(name);
			if (val !== '' && typeof defaultValue !== 'undefined') {
				val = defaultValue;
			}
			//mmcore.SetPersCriterion(name, storage.getItem(name) || defaultValue || '');
			mmcore.SetPersCriterion(name, val);
		}
	}()
)
;

//Example
(function my_pc() {
	mmcore.createPC('my_pc', 333);
	mmcore.AddDocLoadHandler(function () {
		if (/*my condition*/ true) {
			mmcore.SetPersCriterion('my_pc', 1);
			//_listDefPC[name].resolve(val);
			//_listDefPC[name] = mmcore.Deferred();
			//mmcore.listDefPC[name] = _listDefPC[name].promise();
		}
	});
}());



