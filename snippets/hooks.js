/***
 * @name hooks
 * @version 0.0.2
 */
(function () {
    mmcore._AddRenderer = (function (fn) {
        return function () {
            var data = {
                arg: arguments
            };
            var out = fn.apply(this, arguments);
            mmcore.trigger('core_add_renderer', data);
            return out;
        };
    }(mmcore._AddRenderer));
    mmcore._Clear = (function (fn) {
        return function () {
            var data = {
                arg: arguments
            };
            var out = fn.apply(this, arguments);
            mmcore.trigger('core_clear', data);
            return out;
        };
    }(mmcore._Clear));
}());
