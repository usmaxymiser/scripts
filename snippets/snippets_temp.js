 
/* !! @name library @version 0.0.2 *//**
 * Created by ocherkashenko on 07.07.2015.
 * @name library
 * @version 0.0.2
 */
(function () {
    if (typeof mmcore.snippets == 'undefined') {
        mmcore.snippets = {};
    }
    var namespace = mmcore.snippets;
    var class2type = {};

    namespace.extend = function () {
        var options, name, target = arguments[0] || {},
            i = 1,
            length = arguments.length;
        // Extend jQuery itself if only one argument is passed
        if (i === length) {
            target = this;
            i--;
        }

        for (; i < length; i++) {
            if ((options = arguments[i]) != null) {
                for (name in options) {
                    target[name] = options[name];
                }
            }
        }
        // Return the modified object
        return target;
    };
    namespace.extend({
        /**
         * @param obj
         * @param callback
         * @returns {*}
         */
        each: function (obj, callback) {
            var i = 0,
                length = obj.length,
                isArray = isArraylike(obj);

            if (isArray) {
                for (; i < length; i++) {
                    if (callback.call(obj[i], i, obj[i]) === false) {
                        break;
                    }
                }
            } else {
                for (i in obj) {
                    if (callback.call(obj[i], i, obj[i]) === false) {
                        break;
                    }
                }
            }

            return obj;
        },
        /**
         * @param {*} itemToCheck
         * @param {Array} arr
         * @returns {boolean}
         */
        inArray: function (elem, list) {
            var i = 0,
                len = list.length;
            for ( ; i < len; i++ ) {
                if ( list[i] === elem ) {
                    return i;
                }
            }
            return -1;
        },
        /**
         * @param elems
         * @param callback
         * @param invert
         * @returns {Array}
         */
        grep: function (elems, callback, invert) {
            var callbackInverse,
                matches = [],
                i = 0,
                length = elems.length,
                callbackExpect = !invert;

            // Go through the array, only saving the items
            // that pass the validator function
            for (; i < length; i++) {
                callbackInverse = !callback(elems[i], i);
                if (callbackInverse !== callbackExpect) {
                    matches.push(elems[i]);
                }
            }
            return matches;
        },
        /**
         * @param obj {Object}
         * @returns string
         */
        type: function (obj) {
            return obj == null ?
                String(obj) :
            class2type[Object.prototype.toString.call(obj)] || 'object';
        }
    });

    // Populate the class2type map
    namespace.each('Boolean Number String Function Array Date RegExp Object Error'.split(' '),
        function (i, name) {
            class2type['[object ' + name + ']'] = name.toLowerCase();
        });

    function isArraylike(obj) {

        // Support: iOS 8.2 (not reproducible in simulator)
        // `in` check used to prevent JIT error (gh-2145)
        // hasOwn isn't used here due to false negatives
        // regarding Nodelist length in IE
        var length = 'length' in obj && obj.length,
            type = namespace.type(obj);

        if (type === 'function' || obj != null && obj === obj.window) {
            return false;
        }

        return type === 'array' || length === 0 ||
            typeof length === 'number' && length > 0 && ( length - 1 ) in obj;
    }
}()); 
/* !! @name Style @version 0.1.8 *//**
 *
 * @name Style
 * @version 0.1.8
 *
 */
(function (namespace) {
	namespace.Style = function (css) {
		var style;
		var uniqueId = function (length) {
			var id = '';
			do {
				id += Math.random().toString(36).substr(2);
			} while (id.length < length);
			return id.substr(-length);
		};

		var init = function () {
			style = document.createElement("style");
			style.type = "text/css";
			style.media = "screen";
			style.className = 'mm_' + uniqueId(15);
			document.getElementsByTagName('head')[0].appendChild(style);
			attach(css);
		};

		var getStyle = function (style) {
			if (style && !style.parentNode) {
				return document.querySelector('.' + style.className);
			}
			return style;

		};
		var attach = function (css) {
			style = getStyle(style);
			if (style && css) {
				if (style.styleSheet) {
					style.styleSheet.cssText += css;
				} else {
					style.innerHTML += css;
				}
			}
		};

		var detach = function () {
			style = getStyle(style);
			if (style) {
				style.parentNode.removeChild(style);
				style = null;
			}
		};

		init();

		return {
			attach: attach,
			detach: detach
		}
	}
}(mmcore.snippets)); 
/* !! @name Deferred @version 0.0.6 *//**
 * Essentials - Deferred
 *
 * A handy tool for organizing waiting for something.
 *
 * @name Deferred
 * @version 0.0.6
 *
 *
 * @author a.porohnya@gmail.com (Aleksey Porokhnya)
 */
(function ($) {
	var Deferred, PENDING, REJECTED, RESOLVED, after, execute, flatten, has, isArguments, isPromise, wrap, _when, _reduce,
		__slice = [].slice;

	PENDING = "pending";

	RESOLVED = "resolved";

	REJECTED = "rejected";

	_reduce = function (callback /*, initialValue*/) {
		if (null === this || 'undefined' === typeof this) {
			throw new TypeError('_reduce called on null or undefined');
		}
		if ('function' !== typeof callback) {
			throw new TypeError(callback + ' is not a function');
		}
		var t = Object(this), len = t.length >>> 0, k = 0, value;
		if (arguments.length >= 2) {
			value = arguments[1];
		} else {
			while (k < len && !k in t) k++;
			if (k >= len)
				throw new TypeError('Reduce of empty array with no initial value');
			value = t[k++];
		}
		for (; k < len; k++) {
			if (k in t) {
				value = callback(value, t[k], k, t);
			}
		}
		return value;
	};

	has = function (obj, prop) {
		return obj != null ? obj.hasOwnProperty(prop) : void 0;
	};

	isArguments = function (obj) {
		return has(obj, 'length') && has(obj, 'callee');
	};

	isPromise = function (obj) {
		return has(obj, 'promise') && typeof (obj != null ? obj.promise : void 0) === 'function';
	};

	flatten = function (array) {
		if (isArguments(array)) {
			return flatten(Array.prototype.slice.call(array));
		}

		if ($.type(array) != 'array') {
			return [array];
		}
		return _reduce.call(array, function (memo, value) {
			if ($.type(value) == 'array') {
				return memo.concat(flatten(value));
			}
			memo.push(value);
			return memo;
		}, []);
	};

	after = function (times, func) {
		if (times <= 0) {
			return func();
		}
		return function () {
			if (--times < 1) {
				return func.apply(this, arguments);
			}
		};
	};

	wrap = function (func, wrapper) {
		return function () {
			var args;
			args = [func].concat(Array.prototype.slice.call(arguments, 0));
			return wrapper.apply(this, args);
		};
	};

	execute = function (callbacks, args, context) {
		var callback, _i, _len, _ref, _results;
		_ref = flatten(callbacks);
		_results = [];
		for (_i = 0, _len = _ref.length; _i < _len; _i++) {
			callback = _ref[_i];
			if (Object.prototype.toString.call(args) !== '[object Arguments]' && args.length === undefined) {
				args.length = 0;
			}
			_results.push(callback.call.apply(callback, [context].concat(__slice.call(args))));
		}
		return _results;
	};

	/**
	 * @name Deferred
	 * @namespace
	 * @returns {Deferred}
	 * @constructor
	 */
	Deferred = function () {
		var candidate, close, closingArguments, doneCallbacks, failCallbacks, progressCallbacks, state;
		state = PENDING;
		doneCallbacks = [];
		failCallbacks = [];
		progressCallbacks = [];
		closingArguments = {
			'resolved': {},
			'rejected': {},
			'pending': {}
		};
		/**
		 * Promise Object
		 *
		 * @type {Promise}
		 * @param candidate
		 * @returns {{}}
		 */
		this.promise = function (candidate) {
			var pipe, storeCallbacks;
			candidate = candidate || {};
			candidate.state = function () {
				return state;
			};
			storeCallbacks = function (shouldExecuteImmediately, holder, holderState) {
				return function () {
					if (state === PENDING) {
						holder.push.apply(holder, flatten(arguments));
					}
					if (shouldExecuteImmediately()) {
						execute(arguments, closingArguments[holderState]);
					}
					return candidate;
				};
			};
			candidate.done = storeCallbacks((function () {
				return state === RESOLVED;
			}), doneCallbacks, RESOLVED);
			candidate.fail = storeCallbacks((function () {
				return state === REJECTED;
			}), failCallbacks, REJECTED);
			candidate.progress = storeCallbacks((function () {
				return state !== PENDING;
			}), progressCallbacks, PENDING);
			candidate.always = function () {
				var _ref;
				return (_ref = candidate.done.apply(candidate, arguments)).fail.apply(_ref, arguments);
			};
			pipe = function (doneFilter, failFilter, progressFilter) {
				var filter, master;
				master = new Deferred();
				filter = function (source, funnel, callback) {
					if (!callback) {
						return candidate[source](master[funnel]);
					}
					return candidate[source](function () {
						var args, value;
						args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
						value = callback.apply(null, args);
						if (isPromise(value)) {
							return value.done(master.resolve).fail(master.reject).progress(master.notify);
						} else {
							return master[funnel](value);
						}
					});
				};
				filter('done', 'resolve', doneFilter);
				filter('fail', 'reject', failFilter);
				filter('progress', 'notify', progressFilter);
				return master;
			};
			candidate.pipe = pipe;
			candidate.then = pipe;
			if (candidate.promise == null) {
				candidate.promise = function () {
					return candidate;
				};
			}
			return candidate;
		};
		this.promise(this);
		candidate = this;
		close = function (finalState, callbacks, context) {
			return function () {
				if (state === PENDING) {
					state = finalState;
					closingArguments[finalState] = arguments;
					execute(callbacks, closingArguments[finalState], context);
					return candidate;
				}
				return this;
			};
		};
		this.resolve = close(RESOLVED, doneCallbacks);
		this.reject = close(REJECTED, failCallbacks);
		this.notify = close(PENDING, progressCallbacks);
		this.resolveWith = function (context, args) {
			return close(RESOLVED, doneCallbacks, context).apply(null, args);
		};
		this.rejectWith = function (context, args) {
			return close(REJECTED, failCallbacks, context).apply(null, args);
		};
		this.notifyWith = function (context, args) {
			return close(PENDING, progressCallbacks, context).apply(null, args);
		};
		return this;
	};

	/**
	 * Provides a way to execute callback functions based on one or more objects, usually Deferred objects that represent asynchronous events.
	 *
	 * @returns {Promise}
	 * @private
	 */
	_when = function () {
		var def, defs, finish, resolutionArgs, trigger, _i, _len;
		defs = flatten(arguments);
		if (defs.length === 1) {
			if (isPromise(defs[0])) {
				return defs[0];
			} else {
				return (new Deferred()).resolve(defs[0]).promise();
			}
		}
		trigger = new Deferred();
		if (!defs.length) {
			return trigger.resolve().promise();
		}
		resolutionArgs = [];
		finish = after(defs.length, function () {
			return trigger.resolve.apply(trigger, resolutionArgs);
		});

		$.each(defs, function (index, def) {
			if (isPromise(def)) {
				return def.done(function () {
					var args;
					args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
					resolutionArgs[index] = args.length > 1 ? args : args[0];
					return finish();
				});
			} else {
				resolutionArgs[index] = def;
				return finish();
			}
		});

		for (_i = 0, _len = defs.length; _i < _len; _i++) {
			def = defs[_i];
			isPromise(def) && def.fail(trigger.reject);
		}
		return trigger.promise();
	};


	/**
	 * Returns new Deferred object
	 *
	 * @returns {Deferred}
	 * @constructor
	 */
	mmcore.Deferred = function () {
		return new Deferred();
	};
	mmcore.when = _when;

}(mmcore.snippets)); 
/* !! @name EventManager @version 0.0.1 *//**
 * @name EventManager
 * @version 0.0.1
 *

 */
(function () {
	mmcore.snippets.EventManager = function () {
		var eventPool = {};
		var eventListeners = {};
		this.trigger = function (event_type, event_data, callback) {
			eventPool[event_type] = {
				event_data: event_data,
				callback: callback
			};
			eventListeners[event_type] = eventListeners[event_type] || [];
			for (var i = 0; i < eventListeners[event_type].length; i++) {
				try {
					eventListeners[event_type][i](event_data);
				} catch (e) {
					mmcore.EH && mmcore.EH(e);
				}
			}
			callback && callback();
		};
		this.on = function (event_type, callback) {
			var lastData = eventPool[event_type];
			if (lastData) {
				try {
					callback(lastData.event_data);
				} catch (e) {
					mmcore.EH && mmcore.EH(e);
				}
				setTimeout(function () {
					lastData.callback && lastData.callback();
					lastData.callback = null;
				}, 0);
			}
			eventListeners[event_type] = eventListeners[event_type] || [];
			eventListeners[event_type].push(callback);
		};
	};
	mmcore.snippets.EventManager.call(mmcore);
}()); 
/* !! @name printFix @version 0.0.2 *//**
 * @name printFix
 * @version 0.0.2
 *
 */
(function () {
    function bind(el, func) {
        if (el.addEventListener) {
            el.addEventListener('beforeprint', func, false);
        } else if (el.attachEvent) {
            el.attachEvent('onbeforeprint', func);
        }
    }

    bind(window, function () {
        var nodes = document.querySelectorAll('script[id^="' + mmcore.cprefix + '"][src]'),
            i = nodes.length;
        while (i--) {
            nodes[i].removeAttribute('src');
        }
    });
})(); 
/* !! @name WaitFor @version 0.1.1 *//**
 * Essentials - Wait for
 *
 * A handy tool for organizing waiting for something.
 *
 * @name WaitFor
 * @version 0.1.1
 *
 * @requires mmcore.Deffered
 *
 * @author evgeniy@pavlyuk.me (Evgeniy Pavlyuk), a.porohnya@gmail.com (Aleksey Porokhnya)
 */
(function () {
	'use strict';

	/**
	 * A handy tool for organizing waiting for something.
	 *
	 * @param {function(this:!mmcore.Deferred):boolean} checker A function that controls the state of the waiting.
	 * @param {!Object=} options Optional settings:
	 *     initializer {function(this:mmcore.Deferred)} A function that is called before the first check.
	 *     interval {number} Delay (in milliseconds) between the checks.
	 *     isNotPeriodicallyChecked {boolean} Is the check not called periodically, but only once?
	 *     isNotStoppedOnDocumentReadiness {boolean} Is the waiting not stopped when the document gets ready and the
	 *         waiting is incomplete?
	 *     timeout {number} A period of time (in milliseconds) after which the waiting fails.
	 *
	 * @return {!mmcore.Deferred#Promise} The promise of the waiting.
	 *
	 * @example
	 * // Periodically check for the client jQuery arrival.
	 * mmcore.waitFor(function() {
   *    // Successfully complete the waiting when the client jQuery arrives.
   *    // The waiting stops if the client jQuery does not arrive when the document gets ready.
   *   return (typeof jQuery === 'function');
   * }).done(function() {
   *   // The client jQuery has arrived.
   * }).fail(function() {
   *   // The client jQuery has not been arrived when the document got ready.
   * });
	 *
	 * @example
	 *  // Periodically check for the client jQuery arrival.
	 * mmcore.waitFor(function() {
   *   // The checker has direct access to the deferred object, so the waiting can be completed by hand.
   *   // It gives more control over the waiting, and allows to complete the waiting with custom arguments.
   *   if (typeof jQuery === 'function') {
   *     this.resolve(jQuery);
   *   }
   * }).done(function(jQuery) {
   *   // The client jQuery has arrived.
   * });
	 * @example
	 * mmcore.waitFor('#footer').done()....
	 */
	mmcore.waitFor = function (checker, options) {
		var check;
		var waiting;

		//If checker is selector find it
		if (typeof checker == 'string') {
			var selector = checker;
			checker = function () {
				return !!document.querySelector(selector);
			}
		}

		// Manages the state of the event that is expected to happen.
		// Learn more at http://api.jquery.com/category/deferred-object/
		waiting = mmcore.Deferred();

		// The handy method is used instead of copying and pasting the check below.
		waiting.isComplete = function () {
			return (waiting.state() !== 'pending');
		};

		// Extend the default options with the given ones (if any).
		options = options || {};
		options.interval = options.interval || 50;
		options.isNotStoppedOnDocumentReadiness = options.isNotStoppedOnDocumentReadiness || false;
		options.isNotPeriodicallyChecked = options.isNotPeriodicallyChecked || false;


		// Calls the checker.
		// If not prevented by isNotPeriodicallyChecked option, provides periodical calling of itself.
		// If not prevented by isNotStoppedOnDocumentReadiness option, stops the waiting if it is not complete when the
		// document is ready.
		check = function () {
			if (waiting.isComplete()) {
				return;
			}

			// Successfully completes waiting if the checker returns true.
			// Note, that the checker has direct access to the deferred object, so the waiting can be completed in the checker
			// by calling resolve or reject method of the deferred object.
			// Checking for the return value is done to provide an easy and comfortable way to complete the waiting from the
			// checker.
			if (checker.call(waiting)) {
				waiting.resolve();
			}

			if (waiting.isComplete()) {
				return;
			}

			if (!options.isNotStoppedOnDocumentReadiness
				&& ((mmcore.$ && mmcore.$.isReady) || (!mmcore.$ && document.readyState === "complete"))) {
				// Stop the waiting if it is incomplete when the document is ready.
				waiting.reject();
			} else if (!options.isNotPeriodicallyChecked) {
				// Provide periodical calling of the checking function.
				setTimeout(check, options.interval);
			}
		};

		// If there is the initializer function, call it before the first check.
		if (options.hasOwnProperty('initializer')) {
			options.initializer.call(waiting);
		}

		check();

		if (!waiting.isComplete()) {
			if (options.hasOwnProperty('timeout')) {
				// Stop checking after the specified period of time.
				(function () {
					var timeoutId = setTimeout(waiting.reject, options.timeout);
					waiting.always(function () {
						clearTimeout(timeoutId);
					});
				}());
			}

			if (!options.isNotStoppedOnDocumentReadiness) {
				if (!options.isNotPeriodicallyChecked) {
					// If the checker is periodically called, and the waiting stops if it is incomplete when the document is
					// ready, call the checker for the last time exactly at the moment when the document gets ready.
					mmcore.AddDocLoadHandler(check);
					//$(document).ready(check);
				} else {
					// If the checker is not called periodically, but only once, stop the waiting when the document gets ready.
					mmcore.AddDocLoadHandler(waiting.reject);
					//$(document).ready(waiting.reject);
				}
			}
		}

		return waiting.promise();
	};
}());
 
/* !! @name Request @version 0.0.2 *//**
 * @name Request
 * @version 0.0.2
 *
 */
(function (namespace, Deferred) {
	var ACTION_TRACKING_PAGE_ID = 'mmevents'; //Default page
	var REQUEST_TIMEOUT_EVENTS = 2E3;  // In milliseconds.
	var REQUEST_TIMEOUT_PAGES = 1E4;  // In milliseconds.

	namespace.request = function (pageId, isSynchronous, timeout) {
		var request = Deferred();

		pageId = pageId || ACTION_TRACKING_PAGE_ID;
		timeout = timeout || (pageId == ACTION_TRACKING_PAGE_ID ? REQUEST_TIMEOUT_EVENTS : REQUEST_TIMEOUT_PAGES);

		namespace.SetPageID(pageId);
		namespace._async = !isSynchronous;
		namespace.CGRequest(request.resolve);
		setTimeout(function () {
			request.reject('timeout error');
		}, timeout);
		return request.promise()
	};
}(mmcore, mmcore.Deferred)); 
/* !! @name hooks @version 0.0.2 *//***
 * @name hooks
 * @version 0.0.2
 */
(function () {
    mmcore._AddRenderer = (function (fn) {
        return function () {
            var data = {
                arg: arguments
            };
            var out = fn.apply(this, arguments);
            mmcore.trigger('core_add_renderer', data);
            return out;
        };
    }(mmcore._AddRenderer));
    mmcore._Clear = (function (fn) {
        return function () {
            var data = {
                arg: arguments
            };
            var out = fn.apply(this, arguments);
            mmcore.trigger('core_clear', data);
            return out;
        };
    }(mmcore._Clear));
}());
 
/* !! @name Campaign @version 0.1.6 *//**
 * Maxymiser Core - Campaign
 *
 * Campaign constructor that provides basic functionality for campaign development.
 *
 * @name Campaign
 * @version 0.1.6
 *
 * @requires mmcore._testContent
 * @requires mmcore.snippets.Style
 * @requires mmcore.snippets.inArray
 * @requires mmcore.snippets.grep
 */
(function ($) {
	'use strict';
	/**
	 * @type {Campaign}
	 */

	/**
	 * Campaign constructor that provides basic functionality for campaign development.   *
	 * Documentation https://bitbucket.org/gamingteam/essentials-campaign
	 *
	 *
	 * @name Campaign
	 * @namespace
	 * @constructor
	 *
	 * @param {string} name Campaign name from the UI.
	 * @param {string} prefix Prefix of elements.
	 */
	mmcore.Campaign = function (name, prefix) {
		/**
		 * Self
		 * @type {Campaign}
		 * @private
		 */
		var _campaign = this;
		/**
		 * Campaign name.
		 * @type {string}
		 */
		_campaign.name = name;
		/**
		 * Names of campaign elements.
		 * @type {!Array.<string>}
		 */

		_campaign.elementNames = [];

		/**
		 * Contents of campaign elements.
		 * @type {!Object.<string>}
		 */
		_campaign.contents = {};

		/**
		 * Campaign type and number prefix.
		 * @type {string}
		 */
		_campaign.prefix = prefix || 'mm_';

		var initListeners = function(){
			/**
			 *
			 * Catch styles of campaign
			 */

			mmcore.on('core_request_complete', function (data) {
				var elements = _campaign.getElements();
				for (var i = 0; i < elements.length; i++) {
					var element = elements[i];
					if (data.testContent[element]) {
						var arrCss = data.testContent[element].c;
						if (arrCss) {
							_campaign.contents[element] = arrCss.join();
							data.testContent[element].c = [];
						}
					}
				}
			});
			/**
			 * Marks the campaign elements as rendered.
			 */
			mmcore.on('core_add_renderer', function (data) {
				var elementName = data.arg[0];
				var elements = _campaign.getElements();
				if (elements.length) {
					if ($.inArray(elementName, elements) != -1) {
						//prevent default rendering
						mmcore._r_mbs[elementName] = 1;
						_campaign.elementNames.push(elementName)
					}
				}
			});
		};

		mmcore.HideMaxyboxes = function () {
		};

		/**
		 * Hides content by selector.
		 *
		 * @param {string} selector CSS selector(s).
		 * @param {string=} hidingStyle Optional CSS to be applied to the selector.
		 */
		this.hide = function (selector, hidingStyle) {
			hidingStyle = hidingStyle || 'position: relative !important; left:-99999px !important';
			var css = selector + '{' + hidingStyle + '}';
			if (_campaign.refStyle) {
				_campaign.refStyle.attach(css);
			} else {
				_campaign.refStyle = $.Style(css);
			}
		};

		/**
		 * Shows the content previously hidden by `this.hideContent`.
		 */
		this.show = function () {
			_campaign.refStyle && _campaign.refStyle.detach();
			_campaign.refStyle = null;
		};

		/**
		 * Returns generated elements of this campaign from GenInfo
		 *
		 * @returns {Object}
		 */
		this.getElements = function () {
			var experience = _campaign.getExperience();
			var elements = [];
			if (typeof mmcore._testContent == 'object' && experience) {
				for (var element in mmcore._testContent) {
					if (!/undefined|Default/.test(experience[element.toLowerCase().replace(_campaign.prefix, '')])) {
						elements.push(element);
					}
				}
			}
			return elements;
		};

		/**
		 * Returns generated experience of this campaign from GenInfo
		 *
		 * @returns {Object}
		 */
		this.getExperience = function () {
			// Assume that an own property of mmcore.GenInfo reference to a unique object.
			return mmcore.GenInfo.hasOwnProperty(_campaign.name) ?
				mmcore.GenInfo[_campaign.name] : null;
		};

		/**
		 * Returns true if campaign has non default experience
		 *
		 * @return {boolean}
		 */
		this.isDefault = function () {
			return !_campaign.getElements().length;
		};

		/**
		 * Render all or specific campaign elementes.
		 * this in a variant script references to the campaign.
		 */
		this.render = function () {
			var style = $.Style();
			var elementNames = _campaign.elementNames;

			if (arguments.length) {
				elementNames = $.grep(arguments, function (elementName) {
					return $.inArray(elementName, elementNames) != -1;
				});
			}
			for (var key in mmcore._renderers) {
				if ($.inArray(key, elementNames) != -1 && typeof mmcore._renderers[key] === 'function') {
					try {
						//Add style
						style.attach(_campaign.contents[key]);
						//Render elements
						mmcore._renderers[key].call(_campaign);
					} catch (err) {
						mmcore.EH(new Error(key + ' ' + err));
					}
				}

			}
		};

		initListeners();

	};
}(mmcore.snippets));