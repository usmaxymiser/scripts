/**
 * @name TestContent
 * @version 0.0.2
 * Page name - [A]ForAllRequestsWithoutEvents[All]
 * Include mask - *
 * Exclude mask - mmevents
 * Output Order (-1)
 * overlay - true
 */
;
(function () {
    var TEST_CONTENT = '_testContent';
    mmcore[TEST_CONTENT] = mmcore[TEST_CONTENT] || {};
    if (typeof tc == 'object' && tc !== window.tc) {
        for (var key in tc) {
            if (tc.hasOwnProperty(key)) {
                mmcore[TEST_CONTENT][key] = tc[key];
            }
        }
        mmcore.trigger('core_request_complete', {
            testContent: tc
        });
    }

}());