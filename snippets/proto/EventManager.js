/* Example EventManager is a collection of observers
 */

(function (Observer) {
    mmcore.snippets = mmcore.snippets || {};
    mmcore.snippets.EventManager = function (options) {
        var eventListeners = {};
        var eventPool = {};
        //options = options || 'once memory stopOnFalse';
        var createObserver = function (name) {
            return eventListeners[name] = eventListeners[name] || new Observer(options);
        };

        this.on = function (name, fn) {
            createObserver(name).add(fn);
            if(typeof eventPool[name] == 'function'){
                setTimeout(function () {
                    eventPool[name]();
                }, 0);
            }
            return this;
        };

        this.off = function (name, fn) {
            if (eventListeners[name]) {
                if (fn) {
                    eventListeners[name].remove(fn);
                } else {
                    eventListeners[name].empty();
                }
            }
            return this;
        };

        this.trigger = function (name, data, callback) {
            createObserver(name).fire(data);
            if(typeof callback == 'function'){
                eventPool[name] = callback;
                eventPool[name]();
            }
            return this;
        }
    }
}(mmcore.snippets.Observer));
