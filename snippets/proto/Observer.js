/**
 * @ version 0.0.1
 *   Possible options:
 *
 *    once:           will ensure the callback list can only be fired once in the triggered event.
 *
 *
 *    memory:         will keep track of previous values and will call any callback functions added
 *                    after the list has been fired right away with the latest 'memorized' values
 *
 *    stopOnFalse:      interrupt callings when a callback returns false
 * @param options {{once:boolean, memory: boolean, stopOnFalse: boolean}|undefined}
 * @constructor
 *
 * after yuicompressor
 */


/*
 once — указывает, что список колбэков может быть выполнен только единожды, второй и последующие вызовы метода fire() будут безрезультатны (как это сделано в объекте deferred), если этот флаг не указан, то можно несколько раз вызывать метод fire().
 memory — указывает, что необходимо запоминать параметры последнего вызова метода fire() (и выполнения колбэков из списка) и немедленно выполнять добавляемые колбэки с соответствующими параметрами, если они добавляются уже после вызова метода fire() (как это сделано в объекте deferred).
 stopOnFalse — указывает, что нужно прекратить выполнение колбэков из списка, если какой-то из них вернул false, в пределах текущей сессии вызова fire(). Следующий вызов метода fire() начинает новую сессию выполнения списка колбэков, и они будут выполняться опять до тех пор, пока один из списка не вернёт false либо пока не закончатся.
 */
(function () {
    mmcore.snippets = mmcore.snippets || {};
    function createOptions(options) {
        var object = {};
        options.replace(/\S+/g, function (flag) {
            object[flag] = true;
        });
        return object;
    }

    mmcore.snippets.Observer = function (options) {
        // Convert options from String-formatted to Object-formatted if needed
        // (we check in cache first)
        options = typeof options === "string" ?
            createOptions(options) : options || {};

        var // Flag to know if list is currently firing
            firing,
        // Last fire value for non-forgettable lists
            memory,
        // Flag to know if list was already fired
            fired,
        // Flag to prevent firing
            locked,
        // Actual callback list
            list = [],
        // Queue of execution data for repeatable lists
            queue = [],
        // Index of currently firing callback (modified by add/remove as needed)
            firingIndex = -1,
        // Fire callbacks
            fire = function () {

                // Enforce single-firing
                locked = options.once;

                // Execute callbacks for all pending executions,
                // respecting firingIndex overrides and runtime changes
                fired = firing = true;
                for (; queue.length; firingIndex = -1) {
                    memory = queue.shift();

                    while (++firingIndex < list.length) {

                        // Run callback and check for early termination
                        if (list[firingIndex].apply(memory[0], memory[1]) === false &&
                            options.stopOnFalse) {

                            // Jump to end and forget the data so .add doesn't re-fire
                            firingIndex = list.length;
                            memory = false;
                        }
                    }
                }

                // Forget the data if we're done with it
                if (!options.memory) {
                    memory = false;
                }

                firing = false;

                // Clean up if we're done firing for good
                if (locked) {

                    // Keep an empty list if we have data for future add calls
                    if (memory) {
                        list = [];

                        // Otherwise, this object is spent
                    } else {
                        list = "";
                    }
                }
            };


        /**
         * Add a callback or a collection of callbacks to the list
         * @param fn {function} Is a callback function.
         */

        this.add = function (fn) {
            if (list) {

                // If we have memory from a past run, we should fire after adding
                if (memory && !firing) {
                    firingIndex = list.length - 1;
                    queue.push(memory);
                }
                list.push(fn);
                if (memory && !firing) {
                    fire();
                }
            }
            return this;
        };

        /**
         * Remove a callback from the list
         * @param fn {function}
         */
        this.remove = function (fn) {
            var index = 0;
            while (index < list.length) {
                if (list[index] === fn) {
                    list.splice(index, 1);
                } else {
                    index++;
                }
            }
        };

        /**
         * Unbinding All callback functions from the event
         */
        this.empty = function () {
            if (list) {
                list = [];
            }
            return this;
        };

        // Disable .fire and .add
        // Abort any current/pending executions
        // Clear all callbacks and values
        this.disable = function () {
            locked = queue = [];
            list = memory = "";
            return this;
        };
        this.disabled = function () {
            return !list;
        };
        /**
         * Blocking obsever and unbinding callback functions from the event
         */
        this.lock = function () {
            locked = true;
            this.empty();
        };

        /**
         * Checking if the state is locked or not:
         */
        this.locked = function () {
            locked = queue = [];
            if (!memory && !firing) {
                list = memory = "";
            }
            return this;
        };

        /**
         * Publishing the event with the parameters.
         * @param context
         * @param args
         */

        this.fireWith = function (context, args) {
            if (!locked) {
                args = args || [];
                args = [context, args.slice ? args.slice() : args];
                queue.push(args);
                if (!firing) {
                    fire();
                }
            }
            return this;
        };
        /**
         * Publishing the event with the parameters.
         * @param arguments
         */
        this.fire = function () {
            this.fireWith(this, arguments);
            return this;
        };
    }
}());
