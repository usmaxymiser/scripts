/***
 * @name Cookie
 * @version 0.0.1
 */

var Cookie = function (name) {
    var shortNameCampaign = '';
    if (name) {
        shortNameCampaign = 't' + name.match(/\d+/);
    }

    /**
     * Retrieves the value of the custom variable from the campaign's persistent storage.
     * @param {String} name
     */
    this.prototype.getData = function (name) {
        name = shortNameCampaign + name;
        mmcore.GetCookie(name, 1);
    };
    /**
     * Saves the value of the custom variable to the campaign's persistent storage,
     * so it can be retrieved on other site pages using the campaign.getData() method.
     * @param {String} name
     * @param value
     * @param days
     */
    this.prototype.setData = function (name, value, days) {
        days = Number(days);
        if (!isNaN(days)) {
            days = 365;
        }
        name = shortNameCampaign + name;
        mmcore.SetCookie(name, value, days, 1);
        delete mmcore._vars[name];

    };

    /**
     * Removes custom variable data from the campaign's persistent storage.
     * @param {String} name
     */
    this.prototype.clearData = function (name) {
        this.setData(name, '', -1, 1);
    };
};

