/**
 * Maxymiser Core - Campaign
 *
 * Campaign constructor that provides basic functionality for campaign development.
 *
 * @name Campaign
 * @version 0.1.6
 *
 * @requires mmcore._testContent
 * @requires mmcore.snippets.Style
 * @requires mmcore.snippets.inArray
 * @requires mmcore.snippets.grep
 */
(function ($) {
	'use strict';
	/**
	 * @type {Campaign}
	 */

	/**
	 * Campaign constructor that provides basic functionality for campaign development.   *
	 * Documentation https://bitbucket.org/gamingteam/essentials-campaign
	 *
	 *
	 * @name Campaign
	 * @namespace
	 * @constructor
	 *
	 * @param {string} name Campaign name from the UI.
	 * @param {string} prefix Prefix of elements.
	 */
	mmcore.Campaign = function (name, prefix) {
		/**
		 * Self
		 * @type {Campaign}
		 * @private
		 */
		var _campaign = this;
		/**
		 * Campaign name.
		 * @type {string}
		 */
		_campaign.name = name;
		/**
		 * Names of campaign elements.
		 * @type {!Array.<string>}
		 */

		_campaign.elementNames = [];

		/**
		 * Contents of campaign elements.
		 * @type {!Object.<string>}
		 */
		_campaign.contents = {};

		/**
		 * Campaign type and number prefix.
		 * @type {string}
		 */
		_campaign.prefix = prefix || 'mm_';

		var initListeners = function(){
			/**
			 *
			 * Catch styles of campaign
			 */

			mmcore.on('core_request_complete', function (data) {
				var elements = _campaign.getElements();
				for (var i = 0; i < elements.length; i++) {
					var element = elements[i];
					if (data.testContent[element]) {
						var arrCss = data.testContent[element].c;
						if (arrCss) {
							_campaign.contents[element] = arrCss.join();
							data.testContent[element].c = [];
						}
					}
				}
			});
			/**
			 * Marks the campaign elements as rendered.
			 */
			mmcore.on('core_add_renderer', function (data) {
				var elementName = data.arg[0];
				var elements = _campaign.getElements();
				if (elements.length) {
					if ($.inArray(elementName, elements) != -1) {
						//prevent default rendering
						mmcore._r_mbs[elementName] = 1;
						_campaign.elementNames.push(elementName)
					}
				}
			});
		};

		mmcore.HideMaxyboxes = function () {
		};

		/**
		 * Hides content by selector.
		 *
		 * @param {string} selector CSS selector(s).
		 * @param {string=} hidingStyle Optional CSS to be applied to the selector.
		 */
		this.hide = function (selector, hidingStyle) {
			hidingStyle = hidingStyle || 'position: relative !important; left:-99999px !important';
			var css = selector + '{' + hidingStyle + '}';
			if (_campaign.refStyle) {
				_campaign.refStyle.attach(css);
			} else {
				_campaign.refStyle = $.Style(css);
			}
		};

		/**
		 * Shows the content previously hidden by `this.hideContent`.
		 */
		this.show = function () {
			_campaign.refStyle && _campaign.refStyle.detach();
			_campaign.refStyle = null;
		};

		/**
		 * Returns generated elements of this campaign from GenInfo
		 *
		 * @returns {Object}
		 */
		this.getElements = function () {
			var experience = _campaign.getExperience();
			var elements = [];
			if (typeof mmcore._testContent == 'object' && experience) {
				for (var element in mmcore._testContent) {
					if (!/undefined|Default/.test(experience[element.toLowerCase().replace(_campaign.prefix, '')])) {
						elements.push(element);
					}
				}
			}
			return elements;
		};

		/**
		 * Returns generated experience of this campaign from GenInfo
		 *
		 * @returns {Object}
		 */
		this.getExperience = function () {
			// Assume that an own property of mmcore.GenInfo reference to a unique object.
			return mmcore.GenInfo.hasOwnProperty(_campaign.name) ?
				mmcore.GenInfo[_campaign.name] : null;
		};

		/**
		 * Returns true if campaign has non default experience
		 *
		 * @return {boolean}
		 */
		this.isDefault = function () {
			return !_campaign.getElements().length;
		};

		/**
		 * Render all or specific campaign elementes.
		 * this in a variant script references to the campaign.
		 */
		this.render = function () {
			var style = $.Style();
			var elementNames = _campaign.elementNames;

			if (arguments.length) {
				elementNames = $.grep(arguments, function (elementName) {
					return $.inArray(elementName, elementNames) != -1;
				});
			}
			for (var key in mmcore._renderers) {
				if ($.inArray(key, elementNames) != -1 && typeof mmcore._renderers[key] === 'function') {
					try {
						//Add style
						style.attach(_campaign.contents[key]);
						//Render elements
						mmcore._renderers[key].call(_campaign);
					} catch (err) {
						mmcore.EH(new Error(key + ' ' + err));
					}
				}

			}
		};

		initListeners();

	};
}(mmcore.snippets));